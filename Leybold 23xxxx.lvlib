﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="21008000">
	<Property Name="Instrument Driver" Type="Str">True</Property>
	<Property Name="NI.Lib.DefaultMenu" Type="Str">dir.mnu</Property>
	<Property Name="NI.Lib.Description" Type="Str">LabVIEW Plug and Play instrument driver for Leybold CENTER THREE with mains cable, EUR version (230003) and US version (235003) and CENTER TWO with mains cable, EUR version (230004) and US version (235004).

Note: For binding information refer to the LEYBOLD CENTER TWO/CENTER THREE operating manual which can be obtained from http://www.leybold.com/vacuumservices.</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)Z!!!*Q(C=\&gt;1R4M.!%)8B&amp;U2"GROAC"P-#:"=U?=+=Y7U#&amp;'Y1024UPI+;;D28#&amp;83%FL@G_'#*#3.##"B-W%Z/V[^IPN7'L&lt;B83O]&gt;4W?H2LY^0,A9E6DV5(RGNY`,,P_\`X'&amp;]`VX\^=7=Y=0R2PYZO`_0`YX^\`-4'H.O4EXZYEW:.3C56F&amp;07PNW9Z%G?Z%G?Z%E?Z%%?Z%%?Z%(OZ%\OZ%\OZ%ZOZ%:OZ%:OZ%&lt;?&gt;H+2CVTEH*6E]73BJ'H3)*E-2=F,YEE]C3@R]&amp;'**`%EHM34?*CCR*.Y%E`C34Q=JM34?"*0YEE]N'K3&lt;$MZHM2$?Q7?Q".Y!E`A95E&amp;HA!1,"9U$JL!5$!9P!E]A3@Q]&amp;;"*`!%HM!4?"B7Y!E]A3@Q""Y/;7=FGG&lt;;S@(12I\(]4A?R_.Y;#X(YXA=D_.R0#QHR_.Y()3TI.-=AJS$H!H/"]@D?0AHR_.Y()`D=4Q-N3PE\=R-GGEHRW.Y$)`B-4S'BR9S0)&lt;(]"A?QU.&lt;'2\$9XA-D_&amp;B+2E?QW.Y$)CR+-P,;'9=;%QS!M0$8\N&lt;L&amp;WF;"*L?`UU&gt;T?K[A:5X6CK'U:V)[AOM/L#K3[)[E3L4K$KR+C_M/K,K)#KB65.62/VZ86$L;G"[KE6N;1[;E(.JU/`??*WO^6GM^&amp;[P&gt;9Q$/L\8KP63MPF5FX8;&lt;&amp;9;$[@\Z^7F_S\&lt;@&lt;_8,LF`=/T:H?8GNX@$%`^68@^_$)]4@F0VO[Z^"O?D4L4_/%RTTF[!V&amp;6R^)!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">553680896</Property>
	<Property Name="NI.Lib.Version" Type="Str">2.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Configure" Type="Folder">
			<Item Name="Configure.mnu" Type="Document" URL="/&lt;instrlib&gt;/Leybold 23xxxx/Public/Configure/Configure.mnu"/>
			<Item Name="Analog Output Mode.vi" Type="VI" URL="../Public/Configure/Analog Output Mode.vi"/>
			<Item Name="Baud Rate.vi" Type="VI" URL="../Public/Configure/Baud Rate.vi"/>
			<Item Name="Correction Factor.vi" Type="VI" URL="../Public/Configure/Correction Factor.vi"/>
			<Item Name="Display Control Digits.vi" Type="VI" URL="../Public/Configure/Display Control Digits.vi"/>
			<Item Name="Error Relay Allocation.vi" Type="VI" URL="../Public/Configure/Error Relay Allocation.vi"/>
			<Item Name="Filter.vi" Type="VI" URL="../Public/Configure/Filter.vi"/>
			<Item Name="Full Scale Range.vi" Type="VI" URL="../Public/Configure/Full Scale Range.vi"/>
			<Item Name="Gas Type.vi" Type="VI" URL="../Public/Configure/Gas Type.vi"/>
			<Item Name="Offset Correction.vi" Type="VI" URL="../Public/Configure/Offset Correction.vi"/>
			<Item Name="Offset Display.vi" Type="VI" URL="../Public/Configure/Offset Display.vi"/>
			<Item Name="Parameter Setup Lock.vi" Type="VI" URL="../Public/Configure/Parameter Setup Lock.vi"/>
			<Item Name="Sensor Control.vi" Type="VI" URL="../Public/Configure/Sensor Control.vi"/>
			<Item Name="Setpoint.vi" Type="VI" URL="../Public/Configure/Setpoint.vi"/>
			<Item Name="Torr Lock.vi" Type="VI" URL="../Public/Configure/Torr Lock.vi"/>
			<Item Name="Unit Of Measurement.vi" Type="VI" URL="../Public/Configure/Unit Of Measurement.vi"/>
			<Item Name="Watchdog Control.vi" Type="VI" URL="../Public/Configure/Watchdog Control.vi"/>
		</Item>
		<Item Name="Action-Status" Type="Folder">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="Action-Status.mnu" Type="Document" URL="/&lt;instrlib&gt;/Leybold 23xxxx/Public/Action-Status/Action-Status.mnu"/>
			<Item Name="Analog Output Mode Status.vi" Type="VI" URL="../Public/Action-Status/Analog Output Mode Status.vi"/>
			<Item Name="Baud Rate Status.vi" Type="VI" URL="../Public/Action-Status/Baud Rate Status.vi"/>
			<Item Name="Continous Mode.vi" Type="VI" URL="../Public/Action-Status/Continous Mode.vi"/>
			<Item Name="Correction Factor Status.vi" Type="VI" URL="../Public/Action-Status/Correction Factor Status.vi"/>
			<Item Name="Degas Status.vi" Type="VI" URL="../Public/Action-Status/Degas Status.vi"/>
			<Item Name="Degas.vi" Type="VI" URL="../Public/Action-Status/Degas.vi"/>
			<Item Name="Display Control Digits Status.vi" Type="VI" URL="../Public/Action-Status/Display Control Digits Status.vi"/>
			<Item Name="Error Relay Allocation Status.vi" Type="VI" URL="../Public/Action-Status/Error Relay Allocation Status.vi"/>
			<Item Name="Filter Status.vi" Type="VI" URL="../Public/Action-Status/Filter Status.vi"/>
			<Item Name="Full Scale Range Status.vi" Type="VI" URL="../Public/Action-Status/Full Scale Range Status.vi"/>
			<Item Name="Gas Type Status.vi" Type="VI" URL="../Public/Action-Status/Gas Type Status.vi"/>
			<Item Name="High Vacuum Circuit Status.vi" Type="VI" URL="../Public/Action-Status/High Vacuum Circuit Status.vi"/>
			<Item Name="High Vacuum Circuit.vi" Type="VI" URL="../Public/Action-Status/High Vacuum Circuit.vi"/>
			<Item Name="Offset Correction Status.vi" Type="VI" URL="../Public/Action-Status/Offset Correction Status.vi"/>
			<Item Name="Offset Display Status.vi" Type="VI" URL="../Public/Action-Status/Offset Display Status.vi"/>
			<Item Name="Parameter Setup Lock Status.vi" Type="VI" URL="../Public/Action-Status/Parameter Setup Lock Status.vi"/>
			<Item Name="Sensor Control Status.vi" Type="VI" URL="../Public/Action-Status/Sensor Control Status.vi"/>
			<Item Name="Setpoint Status (Single Channel).vi" Type="VI" URL="../Public/Action-Status/Setpoint Status (Single Channel).vi"/>
			<Item Name="Setpoint Status.vi" Type="VI" URL="../Public/Action-Status/Setpoint Status.vi"/>
			<Item Name="Torr Lock Status.vi" Type="VI" URL="../Public/Action-Status/Torr Lock Status.vi"/>
			<Item Name="Unit Of Measurement Status.vi" Type="VI" URL="../Public/Action-Status/Unit Of Measurement Status.vi"/>
			<Item Name="Watchdog Control Status.vi" Type="VI" URL="../Public/Action-Status/Watchdog Control Status.vi"/>
		</Item>
		<Item Name="Data" Type="Folder">
			<Item Name="Polymorphic VIs" Type="Folder">
				<Item Name="All Channels.vi" Type="VI" URL="../Public/Data/Polymorphic VIs/All Channels.vi"/>
				<Item Name="One Channel.vi" Type="VI" URL="../Public/Data/Polymorphic VIs/One Channel.vi"/>
			</Item>
			<Item Name="Data.mnu" Type="Document" URL="/&lt;instrlib&gt;/Leybold 23xxxx/Public/Data/Data.mnu"/>
			<Item Name="Pressure Reading.vi" Type="VI" URL="../Public/Data/Pressure Reading.vi"/>
		</Item>
		<Item Name="Utility" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="Test" Type="Folder">
				<Item Name="Test AD Converter.vi" Type="VI" URL="../Public/Utility/Test/Test AD Converter.vi"/>
				<Item Name="Test Display.vi" Type="VI" URL="../Public/Utility/Test/Test Display.vi"/>
				<Item Name="Test EEPROM.vi" Type="VI" URL="../Public/Utility/Test/Test EEPROM.vi"/>
				<Item Name="Test EPROM.vi" Type="VI" URL="../Public/Utility/Test/Test EPROM.vi"/>
				<Item Name="Test IO.vi" Type="VI" URL="../Public/Utility/Test/Test IO.vi"/>
				<Item Name="Test Keyboard.vi" Type="VI" URL="../Public/Utility/Test/Test Keyboard.vi"/>
				<Item Name="Test RAM.vi" Type="VI" URL="../Public/Utility/Test/Test RAM.vi"/>
				<Item Name="Test RS232C.vi" Type="VI" URL="../Public/Utility/Test/Test RS232C.vi"/>
			</Item>
			<Item Name="Utility.mnu" Type="Document" URL="/&lt;instrlib&gt;/Leybold 23xxxx/Public/Utility/Utility.mnu"/>
			<Item Name="Error Query.vi" Type="VI" URL="../Public/Utility/Error Query.vi"/>
			<Item Name="ITR Data Output.vi" Type="VI" URL="../Public/Utility/ITR Data Output.vi"/>
			<Item Name="Reset RS232C.vi" Type="VI" URL="../Public/Utility/Reset RS232C.vi"/>
			<Item Name="Reset.vi" Type="VI" URL="../Public/Utility/Reset.vi"/>
			<Item Name="Revision Query.vi" Type="VI" URL="../Public/Utility/Revision Query.vi"/>
			<Item Name="Save Parameters.vi" Type="VI" URL="../Public/Utility/Save Parameters.vi"/>
			<Item Name="Self-Test.vi" Type="VI" URL="../Public/Utility/Self-Test.vi"/>
			<Item Name="Transmitter Identification.vi" Type="VI" URL="../Public/Utility/Transmitter Identification.vi"/>
		</Item>
		<Item Name="dir.mnu" Type="Document" URL="/&lt;instrlib&gt;/Leybold 23xxxx/Public/dir.mnu"/>
		<Item Name="Close.vi" Type="VI" URL="../Public/Close.vi"/>
		<Item Name="Initialize.vi" Type="VI" URL="../Public/Initialize.vi"/>
		<Item Name="VI Tree.vi" Type="VI" URL="../Public/VI Tree.vi"/>
	</Item>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Default Instrument Setup.vi" Type="VI" URL="../Private/Default Instrument Setup.vi"/>
	</Item>
	<Item Name="Leybold 23xxxx Readme.html" Type="Document" URL="../Leybold 23xxxx Readme.html"/>
</Library>
